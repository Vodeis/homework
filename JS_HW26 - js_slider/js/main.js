"use strict";

const images = document.querySelectorAll('.container img');
const circles = document.querySelectorAll('.buttons li');
const btn_left = document.getElementById('btn-left');
const btn_right = document.getElementById('btn-right');

let currentID = 0;

btn_left.addEventListener('click', ()=> {
    if(currentID === 0) {
        currentID = images.length -1
    } else {
        currentID--
    }
    classChange()
})
btn_right.addEventListener('click', ()=> {
    currentID = (currentID + 1) % images.length;
    classChange()
})
circles.forEach(e=> {
    e.addEventListener('click', e=> {
        currentID = +e.target.getAttribute('data-index');
        classChange()
    })
})
function classChange() {
    images.forEach(e=>e.classList.add('hide'));
    images[currentID].classList.remove('hide');
    circles.forEach(e=>e.classList.remove('active'));
    circles[currentID].classList.add('active');
}