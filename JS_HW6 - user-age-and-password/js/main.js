"use strict";

function createNewUser() {
    let name = prompt('Enter your name?');
    let surname = prompt('Enter your surname');
    let birthday = prompt('Enter your birthday in format dd.mm.yyyy', 'dd.mm.yyyy');
    return {
        firstName: name,
        lastName: surname,
        birthday,
        getAge() {
            let day = +this.birthday.slice(0, 2);
            let month = +this.birthday.slice(3, 5);
            let year = +this.birthday.slice(6, 10);
            let today = new Date();
            let birthDate = new Date(year, month - 1, day);
            let age = today.getFullYear() - birthDate.getFullYear();
            if ((today.getMonth() - birthDate.getMonth()) < 0 || ((today.getMonth() - birthDate.getMonth()) === 0 && today.getDate() < birthDate.getDate())) {
                age--;
            }
            return age
        },
        getPassword() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.slice(6, 10);
        },
        getLogin() {
            return (this.firstName[0] + this.lastName).toLowerCase()
        },
        setFirstName(name) {
            Object.defineProperties(this, { firstName: { value: name } })
        },
        setLastName(surname) {
            Object.defineProperties(this, { lastName: { value: surname } })
        },
        _constructor() {
            Object.defineProperties(this, {
                firstName: {
                    value: name,
                    writable: false,
                    configurable: true,
                }
            })
        }
    }
}

let newUser = createNewUser();
console.log(newUser);
console.log(newUser.getAge());
console.log(newUser.getPassword());