"use strict";

function addArrElementsToPageWithTimer(elementsArr, place = document.body) {
    const ul = document.createElement('ul');
    place.append(ul);
    addArrElementsToPage(elementsArr, ul);
    const timer = document.createElement('div');
    document.body.prepend(timer);
    setTimeout(countDown, 1000, 3, timer);
}
function addArrElementsToPage(elementsArr, place = document.body) {
    place.insertAdjacentHTML('beforeend', `${elementsArr.map(elem => {
        if (Array.isArray(elem)) {
            addArrElementsToPage(elem, place);
        } else {
            return `<li>${elem}</li>`;
        }
    }).join('')}`);
}
function countDown(time, timerPlace ) {
    if (time > 0) {
        timerPlace.textContent = `The page will be cleared in ${time} seconds`;
        time--;
        setTimeout(countDown, 1000, time, timerPlace);
    } else {
        document.querySelectorAll('ul').forEach(elem => elem.remove());
        timerPlace.remove();
    }
}
const townsInArr = ["Kharkiv", ["Kharkiv", "Kiev", ["Borispol", ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"], "Irpin"], "Odessa", "Lviv", "Dnieper"], "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
addArrElementsToPageWithTimer(townsInArr, document.getElementById('div'));