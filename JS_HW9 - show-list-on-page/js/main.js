"use strict";

// Опишіть, як можна створити новий HTML тег на сторінці.
// С помощью метода createElement создаем тег и добавляем его в HTML удобным нам способом(append,prepend,before,after,replaceWith)
// Либо insertAdjacentHTML, в случае если у нас есть готовый фрагмент HTML кода.

// Опишіть, що означає перший параметр функції insertAdjacentHTML і опишіть можливі варіанти цього параметра.
// Первый параметр отвечает за место, где в потоке будет размещен html елемент.
//"beforebegin" – вставит html  перед елементом к которому мы обратились,
//"afterbegin" – вставит html внутрь, в начало елемента к которому мы обратились,
//"beforeend" – вставит html внутрь, в конец елемента к которому мы обратились,
//"afterend" – вставит html после елемента к которому мы обратились.

// Як можна видалити елемент зі сторінки?
// обращаемся к елементу и методом remove() удаляем.



// Функция без доп задания в самом низу кода, закоментированна...


// А вот если с доп. заданием то есть вопросы.
// 1.  Не получается собрать все эти функции в одну(если это нужно делать, не могу понять).
// 2.  И с выводом обратного отсчёта не могу понять, что так. Вроде всё написано верно, а выводит не поочередно, а всё разом.

function addArrElementsToPage(elementsArr, place = document.body) {
    place.insertAdjacentHTML('beforeend', `<ul>${elementsArr.map(elem => {
        if (Array.isArray(elem)) {
            addArrElementsToPage(elem, place)
        } else {
            return `<li>${elem}</li>`
        }
    }).join('')}</ul>`);
    setTimeout(countDown, 1000, 3)
}



const timer = document.createElement('div')
document.body.prepend(timer)
function countDown(s) {
    if (s > 0) {
        timer.textContent = `The page will be cleared in ${s} seconds`
        s--
        setTimeout(countDown, 1000, s)
    } else {
        document.querySelectorAll('ul').forEach(elem => elem.remove());
        timer.remove();
    }
}

// const towns = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
// addArrElementsToPage(towns)
const townsInArr = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"];
addArrElementsToPage(townsInArr, document.getElementById('div'))




// function countDown(s) {
//     if (s < 1) {
//         for (const iterator of document.body.children) {
//             iterator.remove()
//         }
//     } else {
//         timer.textContent = `The page will be cleared in ${s} seconds`
//         s--
//         setTimeout(countDown, 1000, s)
//     }
// }


// const elementsList = document.createElement('ul');
// let n = 3;
// function addArrElementsToPage(elementsArr, place = document.body) {
//     place.append(elementsList);
//     arrRecursion(elementsArr);
//     setTimeout(countDown(), 1000);
//     // setTimeout(() => elementsList.remove(), 3000)
// }
// function arrRecursion(arr) {
//     for (const iterator of arr) {
//         if (Array.isArray(iterator)) {
//             const newArr = iterator;
//             arrRecursion(newArr);
//         } else {
//             const element = document.createElement('li');
//             element.textContent = iterator;
//             elementsList.append(element);
//         }
//     }
// }


// function countDown() {
//     elementsList.insertAdjacentHTML('afterbegin', `<p>The page will be cleared in ${n} seconds </p>`)
//     console.log()
//     n--
//     if (n < 0) {
//         setTimeout(() => elementsList.remove(), 1000);
// }
// }


// function addArrElementsToPage(elementsArr, place = document.body) {
//     const elementsList = document.createElement('ul');
//     place.append(elementsList);
//     for (const iterator of elementsArr) {
//         const element = document.createElement('li');
//         element.textContent = iterator;
//         elementsList.append(element);
//     }
// }


// function addArrElementsToPage(elementsArr, place = document.body) {
//     place.insertAdjacentHTML('beforeend', `<ul>${elementsArr.map(elem => {
//         return `<li>${elem}</li>`
//     }).join('')}</ul>`)
// }


