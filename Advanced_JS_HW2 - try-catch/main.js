"use strict";

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function booksChecker(booksArr, place) {
    place = document.querySelector(place);
    booksArr.forEach(e => {
        try {
            if (!e.author || !e.name || !e.price) {
                throw new Error('Bad value')
            }
            let ul = document.createElement('ul')
            place.append(ul)
            for (const key in e) {
                let li = document.createElement('li')
                li.innerText = `${key}: ${e[key]}`;
                ul.append(li)
            }

        } catch (error) { console.log(error.message) }
    })
}
booksChecker(books, '#root')
