'use strict';

const table = document.createElement('table');
document.body.append(table);
for (let i = 0; i < 30; i++) {
    const tr = document.createElement('tr');
    table.append(tr);
    for (let i = 0; i < 30; i++) {
        const td = document.createElement('td');
        tr.append(td);
        td.classList.add('white');
    }
};
document.body.addEventListener('click', event => {
    if (event.target.tagName === 'BODY') {
        document.querySelectorAll('td').forEach(elem => {
            if (elem.getAttribute('class') === 'white') {
                elem.style.backgroundColor = '#000';
                elem.classList.add('black');
                elem.classList.remove('white');
            } else {
                elem.style.backgroundColor = '#fff';
                elem.classList.add('white');
                elem.classList.remove('black');
            }
        })
    };
    if (event.target.tagName === 'TD') {
        if (event.target.getAttribute('style') === 'background-color: rgb(0, 0, 0);') {
            event.target.style.backgroundColor = '#fff';
            event.target.classList.add('white');
            event.target.classList.remove('black');
        } else {
            event.target.style.backgroundColor = '#000';
            event.target.classList.add('black');
            event.target.classList.remove('white');
        }
    }
});
