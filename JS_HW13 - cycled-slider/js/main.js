// Опишіть своїми словами різницю між функціями setTimeout() і setInterval()`.
// setTimeout() - запланированный вызов функции/кода.
// setInterval() - функция/код вызывается с указанным интервалом.

// Що станеться, якщо в функцію setTimeout() передати нульову затримку? Чи спрацює вона миттєво і чому?
// Функция сработает после того как будет выполнена основная часть кода.

// Чому важливо не забувати викликати функцію clearInterval(), коли раніше створений цикл запуску вам вже не потрібен?
// Чтобы цыкл вызова не повторялся, в случае если он нам ужене нужен.

"use strict";
let slides = document.querySelectorAll('.images-wrapper .image-to-show');
let currentSlide = 0;
let slideInterval = setInterval(nextSlide, 3000);
let ms = 300
const timer = document.querySelector('#timer');
document.body.addEventListener('DOMContentLoaded', countDown(ms, timer))
document.querySelector('.nav-images').addEventListener('click', slidesManagment)

function nextSlide() {
    // fadeOut(slides[currentSlide], 500)
    slides[currentSlide].className = 'image-to-show';
    currentSlide = (currentSlide + 1) % slides.length;
    // fadeIn(slides[currentSlide], 500, 'block')
    slides[currentSlide].className = 'image-to-show active';
    countDown(300, timer);
}
function slidesManagment(event) {
    if (event.target.innerText === '❮ previous') {
        if (currentSlide === 0) {
            currentSlide = slides.length;
        }
        slides.forEach(elem => {
            elem.classList.remove('active');
        })
        currentSlide--
        slides[currentSlide].classList.add('active')
    } else if (event.target.innerText === 'Прекратить') {
        event.target.innerText = 'Возобновить показ';
        clearInterval(slideInterval);
        timer.innerText = timer.innerText;
    } else if (event.target.innerText === 'Возобновить показ') {
        event.target.innerText = 'Прекратить';
        slideInterval = setInterval(nextSlide, 3000);
        countDown(ms, timer);
    } else if (event.target.innerText === 'next ❯') {
        slides.forEach(elem => {
            elem.classList.remove('active');
        })
        currentSlide = (currentSlide + 1) % slides.length;
        slides[currentSlide].classList.add('active')
    }
}
function countDown(time, timerPlace) {
    if (time > 0) {
        if (document.getElementById('slides-stop').innerText === 'Возобновить показ') {
            // ms = time
        } else {
            time = String(time);
            if (time.length < 3) {
                time = '0' + time
            }
            timerPlace.textContent = `${time.slice(0, 1)}s ${time.slice(1,3)}ms`;
            time--;
            setTimeout(countDown, 10, time, timerPlace);
        }
    }
}
function fadeOut (el, timeout) {
    el.style.opacity = 1;
    el.style.transition = `opacity ${timeout}ms`;
    el.style.opacity = 0;
  
    setTimeout(() => {
      el.style.display = 'none';
    }, 1);
  };
function fadeIn(el, timeout, display) {
    el.style.opacity = 0;
    el.style.display = display || 'block';
    el.style.transition = `opacity ${timeout}ms`;
    setTimeout(() => {
        el.style.opacity = 1;
    }, 10);
};
