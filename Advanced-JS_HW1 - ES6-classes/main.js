class Employee {
    #name;
    #age;
    #salary;
    constructor(name, age, salary) {
      this.#name = name;
      this.#age = age;
      this.#salary = salary;
    }
    set name(value) {
      this.#name = value;
    }
    get name() {
      return this.#name;
    }
    set age(value) {
      this.#age = value;
    }
    get age() {
      return this.#age;
    }
    set salary(value) {
      this.#salary = value;
    }
    get salary() {
      return this.#salary;
    }
  }

  class Programmer extends Employee {
    #lang;
    constructor(name, age, salary, lang) {
      super(name, age, salary);
      this.#lang = lang;
    }
    set lang(value) {
      this.#lang = value;
    }
    get lang() {
      return this.#lang;
    }
    get salary() {
      return super.salary * 3;
    }
  }

  const developer1 = new Programmer("Alex", 30, 2500, "PHP");
  const developer2 = new Programmer("Roman", 35, 2800, "C#");
  const developer3 = new Programmer("Bogdan", 24, 3000, "JAVA");
  console.log(developer1);
  console.log(developer1.salary);
  console.log(developer2);
  console.log(developer2.salary);
  console.log(developer3);
  console.log(developer3.salary);
