"use strict";
const p = document.createElement('p');
const form = document.createElement('form');
const input = document.createElement('input');
p.innerText = 'Price ';
input.type = 'text';
document.body.append(form);
form.append(p);
p.append(input);
input.addEventListener('focus', () => {
    input.style.outline = 'none';
    input.style.borderColor = 'green';
    input.style.boxShadow = '1px 1px 3px 1px green';
})
input.addEventListener('blur', () => {
    if (input.value > 0) {
        if (document.querySelector('span') !== null) {
            document.querySelector('span').remove();
        }
        p.insertAdjacentHTML('beforebegin',
            `<span>Текущая цена: <span style="color: green;">${input.value}</span>
        <span style="margin-left: 20px; cursor: pointer; opacity: 0.5" class='remove'>X</span>
        </span>`);
        input.value = '';
        document.querySelector('.remove').addEventListener('click', () => {
            document.querySelectorAll('span').forEach(elem => elem.remove())
        })
    } else {
        if (document.querySelector('span') !== null) {
            document.querySelector('span').remove();
        }
        input.style.borderColor = 'red';
        input.style.boxShadow = '1px 1px 3px 1px red';
        p.insertAdjacentHTML("afterend", '<span style="color: red">Please enter correct price</span>')
    }
})

