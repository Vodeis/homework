"use strict";

const task4 = {
    name: 'Serhii',
    surname: 'Siedov',
    profession: 'In the future, the frontend developer',
    parents: {
        name: 'Serhii',
        surname: 'Siedov',
        profession: 'Almost a frontend developer',
        parents: {
            name: 'Serhii',
            surname: 'Siedov',
            profession: 'Frontend developer'
        },
    },
}
function cloneObject(object) {
    const newObj = {}
    for (const elem in object) {
        if (typeof object[elem] === 'object') {
            newObj[elem] = cloneObject(object[elem])
        }
        else {
            newObj[elem] = object[elem]
        }
    }
    return newObj
}



const newObj = cloneObject(task4);
console.log(newObj)



