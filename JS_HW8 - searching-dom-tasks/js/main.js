"use stirct";

// Опишіть своїми словами що таке Document Object Model (DOM)
// DOM - это интерфейс позволяющий программам и скриптам получить доступ к содержимому HTML документа. 
// А также изменять содержимое, оформление и структуру этого документа.

// Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML - даёт доступ ко всей строке внутри HTML кода, с тегом, классом, атрибутами и т.д.
// innerText - выводит только контент.

// Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// По айди, классу, атрибуту, тегу, с помощью селекторов. Как по мне нет однозначного ответа какой способ лучше.
// Выбор способа зависит от контекста задачи и количества елементов с которыми нужно взаимодействовать.
// query - мне кажуться прям универсальными, ну и по айди если работать точечно.


document.querySelectorAll('p').forEach(elem => { elem.style.backgroundColor = '#ff0000' });
// document.querySelectorAll('p').forEach(elem => { elem.style.opacity = '1' }); 
// Не уверен, что стоит это делать, пожалуй оставлю закомментированным)


const optionsList = document.getElementById('optionsList');
console.log(optionsList)
console.log(optionsList.parentNode)
console.log(optionsList.childNodes) //Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
// не совсем понимаю, что означает "вивести в консоль назви та тип нод"
console.log(optionsList.children) // Возможно так правильнее.


document.getElementById('testParagraph').innerText = 'This is a paragraph'; // Встановіть в якості контента елемента з класом testParagraph наступний параграф
// опечатка в задании, у елемента задан айди, а не класс.

console.log(document.querySelector('.main-header').children);
for (const elem of document.querySelector('.main-header').children) {
    elem.classList.add('nav-item')
} 

document.querySelectorAll('.section-title').forEach(elem => elem.classList.remove('section-title'))
