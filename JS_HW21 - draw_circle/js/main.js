"use strict"


document.querySelector('#btn').addEventListener('click', () => {
    document.querySelector('button').remove();
    const input = document.createElement('input');
    const button = document.createElement('button')
    document.body.append(input);
    document.body.append(button)
    input.type = 'text';
    button.innerText = 'Намалювати';
    button.addEventListener('click', () => {
        document.querySelectorAll('body > *').forEach(elem => {
            elem.remove();
        });
        const wrapper = document.createElement('section')
        document.body.append(wrapper);
        wrapper.style = `border: 1px solid red; display: flex; flex-wrap: wrap; width: ${input.value * 10}px`
        for (let i = 0; i < 100; i++) {
            const circle = document.createElement('div');
                circle.style = `height: ${input.value}px; width: ${input.value}px; border-radius: 50%; 
                background-color: ${'#' + (Math.random().toString(16) + '000000').substring(2, 8).toUpperCase()};`;
                wrapper.append(circle);
        }
        wrapper.addEventListener('click', (event) => {
            if(event.target.tagName === 'DIV') {
                event.target.remove();
            }
        })
    })

})
