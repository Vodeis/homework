"use strict";

const arr =['hello', 'world', 23, '23', null];

function filterBy(arr, types) {
    return arr.filter(function (type) {
        return typeof type != types 
    })
}

const newArr = filterBy(arr, 'string');
console.log(newArr);