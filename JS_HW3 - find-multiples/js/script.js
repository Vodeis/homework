"use strict";

let someNumber = prompt('Enter any number');
while (!isInteger(someNumber) || !someNumber.trim()) {
    someNumber = prompt('Enter any integer number');
}
if (someNumber < 5) {
    console.log("Sorry, no numbers");
} else {
    for (let i = 5; i <= someNumber; i++) {
        if (!(i % 5)) {
            console.log(i);
        }
    }
}

function isInteger(num) {
    if (num % 1 === 0) {
        return true;
    }
    return false;
}

// additional task
//
// let num1 = prompt('Enter first number');
// while (!+num1 || !isInteger(num1)) {
//     alert('You put invalid format of number');
//     num1 = prompt('Enter integer number');
// }
// let num2 = prompt('Enter second number');
// while (!+num2 || !isInteger(num2)) {
//     alert('You put invalid format of number');
//     num2 = prompt('Enter integer number');
// }
// let m = null;
// let n = null;
// if (num1 > num2) {
//     m = num1;
//     n = num2;
// } else {
//     m = num2;
//     n = num1;
// }
// nextLoop:
// for (let i = n; i <= m; i++) {
//     for (let j = 2; j < i; j++) {
//         if (i % j === 0) continue nextLoop;
//     }

//     console.log(i);
// }
// function isInteger(num) {
//     if (num % 1 === 0) {
//         return true;
//     }
//     return false;
// }