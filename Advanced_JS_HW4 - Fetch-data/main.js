"use strict"

async function loadMovies() {
    let response = await fetch('https://ajax.test-danit.com/api/swapi/films');
    let movies = await response.json();
    const container = document.querySelector('.star-wars');
    movies.forEach(movie => {
        let charactersList = document.createElement('ul');
        charactersList.classList.add('lds-dual-ring');
        let li = document.createElement('li');
        let episodNumber = document.createElement('p');
        let movieName = document.createElement('p');
        let content = document.createElement('p');
        episodNumber.textContent = movie.episodeId;
        movieName.textContent = movie.name;
        content.textContent = movie.openingCrawl;
        li.append(episodNumber, movieName, charactersList, content);
        container.append(li);
        movie.characters.forEach(async (characterUrl) => {
            let response = await fetch(characterUrl);
            let characters = await response.json();
            let charLi = document.createElement('li');
            charLi.textContent = characters.name;
            charactersList.append(charLi);
            charactersList.classList.remove('lds-dual-ring');
        })
    })
}
loadMovies()





















// function starWars() {
//     const container = document.querySelector('.star-wars');
//     fetch('https://ajax.test-danit.com/api/swapi/films')
//         .then(res => res.json())
//         .then(res => {
//             let li = '';

//             res.forEach(film => {
//                 let charactersName = '';

//                 film.characters.forEach(person => {
//                     fetch(person)
//                         .then(res => res.json())
//                         .then(res => {
//                             charactersName += `<p>${res.name}</p>`
//                         })
//                 })
//                 console.log(charactersName);

//                 li += `<li>
//                         <p>${film.episodeId}</p>
//                         <p class="film-name">${film.name}</p>
//                         <p>${film.openingCrawl}</p>
//                       </li>`;
//             });
//             container.innerHTML = li;

//         })
// }
// starWars();


// function loadMovies() {
//     const container = document.querySelector('.star-wars');
//     fetch('https://ajax.test-danit.com/api/swapi/films')
//         .then(res => res.json())
//         .then(res => {
//             res.forEach(film => {
//                 container.innerHTML += `<li data-id="${film.episodeId}">
//                         <p>${film.episodeId}</p>
//                         <p class="film-name">${film.name}</p>
//                         <ul class="characters"></ul>
//                         <p>${film.openingCrawl}</p>
//                        </li>`;
//                 let element = document.querySelector(`[data-id='${film.episodeId}'] ul`)

//                 loadCharacters(film.characters, element)

//                 // let element = document.querySelector(`[data-id='${film.episodeId}'] ul`)

//                 // film.characters.forEach(url => {
//                 //     fetch(url).then(res => res.json()).then(character => {
//                 //         element.innerHTML += `<li>${character.name}</li>`;
//                 //         console.log(character.name);
//                 //     })
//                 // })
//             });
//         })
// }

// function loadCharacters(characters, element) {
//     characters.forEach(url => {
//         fetch(url).then(res => res.json()).then(character => {
//             element.innerHTML += `<li>${character.name}</li>`;
//         })
//     })
// }
// loadMovies();



