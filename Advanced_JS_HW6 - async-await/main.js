"use strict"

document.querySelector('#btn-search').addEventListener('click', async (e) => {
    let response = await fetch('https://api.ipify.org/?format=json');
    let ip = await response.json();
    console.log(ip);
    let response2 = await fetch('http://ip-api.com/json/' + ip.ip + '?fields=continent,country,regionName,city,district');
    let data = await response2.json();
    console.log(data);
    e.target.insertAdjacentHTML('afterend',
    `<ul>
        <li>${data.continent}</li>
        <li>${data.country}</li>
        <li>${data.regionName}</li>
        <li>${data.city}</li>
        <li>${data.district === ''? 'no data': data.district}</li>

    </ul>
    ` )
})