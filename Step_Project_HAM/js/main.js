"use strict";

const ourServicesTabs = document.querySelector('.our-services_tabs');
ourServicesTabs.addEventListener('click', (event) => {
    if (event.target.tagName === 'LI') {
        document.querySelector('.our-services_tabs .our-services_tab-active').classList.remove('our-services_tab-active');
        document.querySelector('.our-services_tab-content.our-services_tab-content-active').classList.remove('our-services_tab-content-active');
        document.querySelector(event.target.getAttribute('data-id')).classList.add('our-services_tab-content-active');
        event.target.classList.add('our-services_tab-active');
    }
})
const ourWorkTabs = document.querySelector('.our-work_tabs');
const loadBtn = document.getElementById('our-work_load-btn');
const galleryLoadBtn = document.querySelector('.gallery_load-btn');
ourWorkTabs.addEventListener('click', event => {
    if (event.target.tagName === 'LI') {
        loadBtn.style.display = 'flex';
        document.querySelector('.our-work_tabs .our-work_tab-active').classList.remove('our-work_tab-active');
        document.querySelectorAll('.our-work_img-container img').forEach(e => e.classList.add('hide'));
        document.querySelectorAll(event.target.getAttribute('data-class') + `:not(.our-work_img-load-more)`).forEach(e => e.classList.remove('hide'))
        event.target.classList.add('our-work_tab-active');
        if (event.target.innerText === 'All') {
            document.querySelectorAll('.our-work_img-load-more').forEach(e => e.classList.add('hide'));
        }
    }
})
loadBtn.addEventListener('click', () => {
    document.querySelector('.our-work-container .loader').style.display = 'inline-block';
    setTimeout(isLoader, 2000);
})
galleryLoadBtn.addEventListener('click', () => {
    document.querySelector('.gallery .loader').style.display = 'inline-block';
    setTimeout(() => {
        galleryLoadBtn.style.display = 'none';
        document.querySelector('.gallery .loader').style.display = 'none';
        document.querySelectorAll('.gallery_grid-container .hide').forEach(e => e.classList.remove('hide'))
    }, 2000);
})
function isLoader() {
    loadBtn.style.display = 'none';
    document.querySelector('.our-work-container .loader').style.display = 'none';
    document.querySelectorAll(document.querySelector('.our-work_tab-active').getAttribute('data-class')).forEach(e => e.classList.remove('hide'))
}
const aboutUsTabs = document.querySelectorAll('.about-us_tab');
const aboutUsContent = document.querySelectorAll('.about-us_comment');
let slideId = 2;
document.querySelector('.about-us_tabs-container').addEventListener('click', showTab);
const BTN_PREVIOUS = document.querySelector('.about-us_tabs-container button:first-child');
const BTN_NEXT = document.querySelector('.about-us_tabs-container button:last-child');
BTN_PREVIOUS.addEventListener('click', previousTab);
BTN_NEXT.addEventListener('click', nextTab);



function showTab(event) {
    if (event.target.tagName === 'IMG') {
        document.querySelectorAll('.about-us_tab-active').forEach(e => e.classList.remove('about-us_tab-active'));
        document.querySelectorAll('.about-us_comment-active').forEach(e => e.classList.remove('about-us_comment-active'));
        event.target.classList.add('about-us_tab-active')
        document.querySelector(event.target.getAttribute('data-id')).classList.add('about-us_comment-active')
        slideId = parseInt(event.target.getAttribute('data-index'))
    }
}
function previousTab() {
    aboutUsContent.forEach(e => e.classList.remove('about-us_comment-active'));
    aboutUsTabs.forEach(e => e.classList.remove('about-us_tab-active'));
    if (slideId === 0) {
        slideId = aboutUsTabs.length;
    };
    slideId--;
    aboutUsContent[slideId].classList.add('about-us_comment-active');
    aboutUsTabs[slideId].classList.add('about-us_tab-active');
}
function nextTab() {
    aboutUsContent.forEach(e => e.classList.remove('about-us_comment-active'));
    aboutUsTabs.forEach(e => e.classList.remove('about-us_tab-active'))
    slideId = (slideId + 1) % aboutUsTabs.length;
    aboutUsContent[slideId].classList.add('about-us_comment-active');
    aboutUsTabs[slideId].classList.add('about-us_tab-active')
}


// gallery section

const darkering = document.querySelector('.darkering');
const fullScreen_BTNs = document.querySelectorAll('.gallery_img-hover div:last-child');
fullScreen_BTNs.forEach(e => {
    e.addEventListener('click', e => {
        let currentImg = e.target.closest('.gallery_img').lastElementChild;
        currentImg.classList.add('gallery_img-full-screen');
        darkering.style.display = 'block';
        darkering.addEventListener('click', () => {
            darkering.style.display = 'none';
            currentImg.classList.remove('gallery_img-full-screen');
        })
    })
})

const scaleImg_BTN = document.querySelectorAll('.gallery_img-hover div:first-child');
scaleImg_BTN.forEach(e => {
    e.addEventListener('mouseover', e => {
        let currentImg = e.target.closest('.gallery_img').lastElementChild;
        currentImg.style = 'position: fixed; left: 50%; top: 50%; transform:translate(-50%,-50%); z-index: 998'
    })
})
scaleImg_BTN.forEach(e => {
    e.addEventListener('mouseout', e => {
        let currentImg = e.target.closest('.gallery_img').lastElementChild;
        currentImg.style = ''
    })
})



// event.target.dataSet.data-name