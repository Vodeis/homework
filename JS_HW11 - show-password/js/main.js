// "use strict"

document.querySelector('.password-form').addEventListener('click', (event) => {
    if (event.target.tagName === 'I') {
        document.querySelector('#' + event.target.getAttribute('id')).classList.toggle('fa-eye-slash');
        if (document.querySelector('#' + event.target.getAttribute(`id`)).classList.contains('fa-eye-slash')) {
            document.querySelector(event.target.getAttribute('data-input_id')).type = 'text';
        } else {
            document.querySelector(event.target.getAttribute('data-input_id')).type = 'password';

        }
    }
    if (event.target.tagName === 'BUTTON') {
        event.preventDefault();
        if (document.querySelector('#first_input').value === document.querySelector('#second_input').value) {
            document.querySelector('#confirm-password span').classList.remove('false');
            alert('You are welcome');
        } else {
            document.querySelector('#confirm-password span').classList.add('false');
        }
    }
})

// Дополнительный кусок кода в коментарии, прошу посомотреть как лучше.

// Как вариант для проверки некоретного ввода, в режиме реального времени.
// К заданию не относится, скорее вопрос стоит ли так делать.
// И так же проверка на пустую строку, есть ли лучше решение?


// document.querySelector('.btn').addEventListener('mouseover', () => {
//     if (document.querySelector('#first_input').value.trim() === '') {
//         document.querySelector('#empty').classList.add('false');
//     } else {
//         document.querySelector('#empty').classList.remove('false');
//     };
//     if (document.querySelector('#first_input').value === document.querySelector('#second_input').value) {
//         document.querySelector('#confirm-password span').classList.remove('false');
//     } else {
//         document.querySelector('#confirm-password span').classList.add('false');
//     };
// })