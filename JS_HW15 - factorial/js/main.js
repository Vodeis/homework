"use strict";

let someNumber;
do {
    someNumber = prompt('Enter any value');
}
while (isNaN(someNumber) || !someNumber.trim());
function factorial(n) {
    if (n === 1) {
        return n
    } else {
        return n * factorial(n - 1);
    }
}
console.log(factorial(someNumber));



// another option

// let someNumber = prompt("Write number to count factorial", "");
// while(isNaN(someNumber) || !someNumber.trim()) {
//     someNumber = prompt("Write number to count factorial", `${someNumber}`);
// }

// function factorial(n) {
//     let sum = 1;
//         for (let i = 2; i <= n; i++) {
//             sum = sum * i;
//         }
//         return sum;
// }

// console.log(factorial(someNumber));
