"use strict";

function createNewUser() {
    let name = prompt('Enter your name?');
    let surname = prompt('Enter your surname')
    return {
        firstName: name,
        lastName: surname,
        getLogin() {
           return (this.firstName[0] + this.lastName).toLowerCase()
        },
        setFirstName(name) {
            Object.defineProperties(this, { firstName: { value: name } })
        },
        setLastName(surname) {
            Object.defineProperties(this, { lastName: { value: surname } })
        },
        _constructor() {
            Object.defineProperties(this, {
                firstName: {
                    value: name,
                    writable: false,
                    configurable: true,
                }
            })
        }
    }
}

const newUser = createNewUser();
newUser._constructor();
newUser.getLogin();
console.log(newUser.getLogin());
