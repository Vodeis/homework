"use strict";
const cssTheme = document.getElementById('theme-link');
const themeBtn = document.querySelector('#theme-btn');
const theme = document.querySelector('#theme-link');
// document.addEventListener('DOMContentLoaded', () => {
if (localStorage.getItem('css') !== null) {
    cssTheme.href = localStorage.getItem('css');
}
// })
themeBtn.addEventListener('click', () => {
    if (theme.getAttribute('href') === 'css/light-style.css') {
        theme.href = 'css/dark-style.css';
        localStorage.setItem('css', 'css/dark-style.css')

    } else {
        theme.href = 'css/light-style.css';
        localStorage.setItem('css', 'css/light-style.css')

    }
})