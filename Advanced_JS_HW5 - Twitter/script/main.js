"use strict"

class Card {
    name;
    email;
    userId;
    title;
    text;
    postId;
    constructor({ name, email, title, text, userId, postId }) {
        this.title = title,
            this.text = text,
            this.name = name,
            this.email = email,
            this.userId = userId,
            this.postId = postId
    }
    render(element) {
        element.insertAdjacentHTML('beforeend',
            ` <div data-user-id="${this.userId}" data-post-id="${this.postId}" class="card card-body">
                <p class="btn-delete" onclick="fetchCardDelete(${this.postId})">X</p>
                <img class="edit-card" onclick="showEditModal(${this.postId})" src="img/edit_modify_icon-icons.com_72390.svg" alt="put">
                <a href="#" class="text-dark">${this.name}</a>
                <a href="#" class="text-muted d-block">${this.email}</a>
                <h2>${this.title}</h2>
                <p>${this.text}</p>
              </div>`
        )
    }
}
document.querySelector('.add-card').addEventListener('click', () => {
    document.querySelector('.form__add-card').style.display = 'flex';
    document.getElementById('add-card').style.display = 'block';
    document.getElementById('edit-card').style.display = 'none';
    document.querySelector('.form__add-card input').value = '';
    document.querySelector('.form__add-card textarea').value = '';
}
)
const cardsArr = [];
const wrapper = document.querySelector('.cards-container');
const btnAddCard = document.getElementById('add-card')
const btnEditCard = document.getElementById('edit-card')
async function loadCards() {
    const responseUsers = await fetch('https://ajax.test-danit.com/api/json/users');
    const users = await responseUsers.json();
    const responsePosts = await fetch('https://ajax.test-danit.com/api/json/posts');
    const posts = await responsePosts.json();
    makeCards(posts, users);
    renderCards(wrapper);
    hideLoader();
    btnAddCard.addEventListener('click', addCard)
    btnEditCard.addEventListener('click', editCard)
}

function makeCards(posts, users) {
    posts.forEach(post => {
        if (!!post.userId) {
            const userCard = users.find(user => user.id === post.userId);
            if (userCard.id === post.userId) {
                const cardObj = {
                    name: userCard.name,
                    email: userCard.email,
                    title: post.title,
                    text: post.body,
                    postId: post.id,
                    userId: userCard.id
                }
                let card = new Card(cardObj);
                cardsArr.push(card);
            }
        }
    });
}

function renderCards(wrapper) {
    wrapper.innerHTML = '';
    cardsArr.forEach(async item => {
        await item.render(wrapper);
    })
}

function fetchCardDelete(postId) {
    fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json'
        },
    })
        .then(res => {
            if (!!res.ok) {
                removeItemFromArr(postId)
                renderCards(wrapper)
            }
        })
}

function removeItemFromArr(postId) {
    const indexOfObject = cardsArr.findIndex(object => {
        return object.postId === postId;
    });
    cardsArr.splice(indexOfObject, 1);
}

function hideLoader() {
    document.querySelector('.lds-dual-ring').style.display = 'none';
}

function addCard() {
    document.querySelector('.form__add-card').style.display = 'none'
    const title = document.querySelector('.form__add-card input');
    const text = document.querySelector('.form__add-card textarea');
    const user = searchUserFromId(1)
    const userCard = {
        name: user.name,
        email: user.email,
        title: title.value,
        text: text.value,
        postId: 101,
        userId: user.userId
    }
    const newCard = new Card(userCard)
    cardsArr.unshift(newCard)
    postUserCard(wrapper, userCard)
}

function searchUserFromId(id) {

    return cardsArr.find(card => card.userId === id)
}

function postUserCard(wrapper, userCard) {
    fetch('https://ajax.test-danit.com/api/json/posts', {
        method: 'POST',
        body: JSON.stringify({
            id: userCard.postId,
            userId: userCard.userId,
            title: userCard.title,
            body: userCard.text
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
        .then(res => {
            if (!!res.ok) {
                renderCards(wrapper)
            }
        })
}

function editCard(e) {
    document.querySelector('.form__add-card').style.display = 'none'
    const postId = +e.target.getAttribute('post-id');
    const post = searchPostFromId(postId)
    const title = document.querySelector('.form__add-card input');
    const text = document.querySelector('.form__add-card textarea');
    const editedPost = {
        postId: post.postId,
        userId: post.userId,
        name: post.name,
        email: post.email,
        text: text.value,
        title: title.value
    }
    const editPost = new Card(editedPost)
    replaceItemFromArr(postId, editPost)
    renderCards(wrapper)
    fetchCardEdit(postId, editPost)
    document.getElementById('edit-card').setAttribute('post-id', '');

}

function searchPostFromId(id) {
    return cardsArr.find(card => card.postId === id)
}

function replaceItemFromArr(postId, editPost) {
    const indexOfObject = cardsArr.findIndex(object => {
        return object.postId === postId;
    });
    cardsArr.splice(indexOfObject, 1, editPost);
}

function fetchCardEdit(postId, editPost) {
    fetch(`http://ajax.test-danit.com/api/json/posts/${postId}`, {
        method: 'PUT',
        body: JSON.stringify({
            id: editPost.postId,
            userId: editPost.userId,
            title: editPost.title,
            body: editPost.text
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    })
}

function showEditModal(postId) {
    const post = searchPostFromId(postId)
    document.querySelector('.form__add-card').style.display = 'flex';
    document.getElementById('add-card').style.display = 'none';
    const btnEdit = document.getElementById('edit-card');
    btnEdit.style.display = 'block';
    btnEdit.setAttribute('post-id', postId);
    document.querySelector('.form__add-card input').value = post.title;
    document.querySelector('.form__add-card textarea').value = post.text;

}
loadCards();
