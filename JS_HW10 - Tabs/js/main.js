"use strict"

document.querySelector('.tabs').addEventListener('click', (event)=>{
    if(event.target.tagName === 'LI'){
        document.querySelectorAll('.tabs li.active, .tabs-content li.active').forEach(elem => {
            elem.classList.remove('active');
        })
        event.target.classList.add('active');
        document.querySelector(event.target.getAttribute('data-id')).classList.add('active');
    }
})